module ScoutRFP
  module Helper
    module ContactHelper
      def sanitize_name(name)
        name.gsub('^A-Za-z', '').strip
      end
    end
  end
end