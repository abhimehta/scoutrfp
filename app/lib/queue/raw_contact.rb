require 'rubygems'
require_relative 'contact_queue'

module ScoutRFP
  module Queue
    module RawContact
      extend ContactQueue

      class << self
        def key
          'RAW_CONTACT_QUEUE'
        end
      end

    end
  end
end
