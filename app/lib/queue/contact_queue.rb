require 'rubygems'
require_relative '../../../app/lib/client/redis_adapter'

module ScoutRFP
  module Queue
    module ContactQueue

      def setup
        puts "Deleting List with key: #{key}"
        ScoutRFP::Client::RedisAdapter.instance.redis.del "#{key}"
      end

      def push(object)
        puts "Pushing Contact at the end of Queue: #{key}, transaction_id: #{object[:transaction_id]}"
        ScoutRFP::Client::RedisAdapter.instance.redis.rpush key, object
      end

      def pop
        puts "Fetching Contact at the Start of Queue: #{key}"
        ScoutRFP::Client::RedisAdapter.instance.redis.lpop key
      end

      def empty?
        ScoutRFP::Client::RedisAdapter.instance.redis.llen(key) == 0
      end

      def length
        ScoutRFP::Client::RedisAdapter.instance.redis.llen(key)
      end
    end
  end
end
