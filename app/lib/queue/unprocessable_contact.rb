require 'rubygems'
require_relative 'contact_queue'

module ScoutRFP
  module Queue
    module UnprocessableContact
      extend ContactQueue

      class << self
        def key
          'UNPROCESSABLE_CONTACT_QUEUE'
        end
      end

    end
  end
end