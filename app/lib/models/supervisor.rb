require_relative '../../../app/helper/contact_helper'
module ScoutRFP
  module Models
    class Supervisor

      include ScoutRFP::Helper::ContactHelper

      attr_accessor :first_name, :last_name, :email, :uuid

      def initialize(first_name:, last_name:, email:)
        @first_name = first_name
        @last_name = last_name
        @email = email
        @uuid = get_uuid
      end

      def present?
        @first_name.nil? && @last_name.nil? && @email.nil?
      end

      def get_uuid
        if present?
          # Make HTTP Call to the REST API to search via eamil or lookup in the Cache
        else
          nil
        end

      end
    end
  end
end