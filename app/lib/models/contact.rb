require_relative '../../../app/helper/contact_helper'
module ScoutRFP
  module Models
    class Contact
      include ScoutRFP::Helper::ContactHelper

      attr_accessor :first_name, :last_name, :email,
                    :company_name, :naics_sector,
                    :supervisor

      def initialize(first_name:, last_name:, email:, company_name:, naics_sector_code:, naics_sector_title:, supervisor:)
        @first_name = sanitize_name(first_name)
        @last_name = sanitize_name(last_name)
        @email = email
        @company_name = sanitize_name(company_name)
        @naics_sector_code = sanitize_name(naics_sector_code)
        @naics_sector_title = sanitize_name(naics_sector_title)
        @supervisor = supervisor
      end

      def valid_raw_contact?
        return false if @first_name.nil? || @last_name.nil? || @email.nil? || @naics_sector_code.nil? || @naics_sector_title.nil? || @company_name.nil?
        true
      end

      def created?
        # Make HTTPCall to search the contact via email
        # If true
        #   return object
        # Else
        #   Make POST call to create the contact
      end
    end
  end
end