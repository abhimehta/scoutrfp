=begin
  Parser for NAICS Codes
=end
require 'roo'

module ScoutRFP
  module Parser
    module NAICS
      class << self

        @@sector_code_title_index = {}
        @@sector_title_code_index = {}

        def process
          file_path = File.dirname(File.dirname(File.dirname(File.dirname(File.dirname(File.expand_path(__FILE__)))))) + '/data/2_6_digit_2017_Codes.xlsx'
          xlsx = Roo::Excelx.new(file_path)
          xlsx.each_row_streaming(offset: 2) do |row|
            code = row[1].value
            title = row[2].value
            if !code.nil? && !title.nil?
              @@sector_code_title_index[code.to_s.to_sym] = title
              @@sector_title_code_index[title.to_s.to_sym] = code
            end
          end
        end

        def get_sector_code(sector_titles)
          if sector_titles.nil?
            results = @@sector_code_title_index
          else
            results = {}
            sector_titles.each do |title|
              original_code = @@sector_title_code_index[title.to_s.to_sym].to_s
              codes = original_code.split('-') rescue [original_code]
              all_keys = []
              codes.each do |code|
                all_keys << @@sector_code_title_index.keys.map!(&:to_s).select {|key| key.start_with?(code.to_s)}
              end
              all_keys.flatten.each do |key|
                results[key] = @@sector_code_title_index[key.to_sym]
              end
            end
            results
          end
        end

        def sector_code_title_index
          @@sector_code_title_index
        end

        def sector_title_code_index
          @@sector_title_code_index
        end
      end
    end
  end
end