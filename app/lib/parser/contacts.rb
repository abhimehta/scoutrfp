=begin
  CSV Parser for Contacts.csv
=end

require 'uuid'
require 'csv'
require_relative '../models/supervisor'
require_relative '../models/contact'
require_relative '../../../app/lib/queue/raw_contact'
require_relative '../../lib/indexes/contact'
require_relative '../../lib/indexes/supervisor'

module ScoutRFP
  module Parser
    module Contact
      class << self
        def process(sectors)

          @supervisor_cache = ScoutRFP::Cache::Supervisor.new

          uuid = UUID.new
          file_path = File.dirname(File.dirname(File.dirname(File.dirname(File.expand_path(__FILE__))))) + '/data/contacts.csv'
          CSV.foreach(file_path, headers: true) do |row|
            transaction_id = uuid.generate
            data = row.to_hash
            naics_sector = data["naics_sector"]

            if sectors.keys.include?(naics_sector)
              # Check if Supervisor index present?
              supervisor_email = data["supervisor_email"] || nil
              supervisor = @supervisor_cache.present?(supervisor_email)
              contact = ScoutRFP::Models::Contact.new(
                first_name: data["first_name"],
                last_name: data["last_name"],
                email: data["email"],
                company_name: data["company_name"],
                naics_sector_code: data["naics_sector"],
                naics_sector_title: sectors[naics_sector],
                supervisor: supervisor
              )
              message = { transaction_id: transaction_id, contact: contact }
              if contact.valid_raw_contact?
                # Should have called contact.created? to see if Duplicate Contact exists.
                # Have been Getting 502 from the server randomly.
                # Pushing to RawContact Queue as a means for transparency
                ScoutRFP::Queue::RawContact.push(message)
              else
                # Push to UnprocessableContact - DeadLetterQueue for further analysis
                ScoutRFP::Queue::UnprocessableContact.push(message)
              end
            end
          end
          p "Processed: #{ScoutRFP::Queue::RawContact.length} contacts"
        end
      end
    end
  end
end