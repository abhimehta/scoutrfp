=begin
  Cache to determine if the supervisor is already found.
=end
require_relative '../client/thunderturtle_adapter'
require_relative '../models/supervisor'

module ScoutRFP
  module Cache
    class Supervisor
      attr_accessor :supervisor_cache

      def initialize
        @supervisor_cache = {}
      end

      def present?(email)
        return false if email.nil?
        if !@supervisor_cache.length.zero? && @supervisor_cache[email].present?
          @supervisor_cache[email]
        else
          response = ScoutRFP::Client::ThunderTurtleAdapter.search(email)
          if !response["data"].length.zero?
            last_name = response["data"].first["attributes"]["last_name"]
            first_name = response["data"].first["attributes"]["first_name"]
            email = response["data"].first["attributes"]["email"]
            supervisor = ScoutRFP::Models::Supervisor.new(first_name: first_name, last_name: last_name, email: email)
            @supervisor_cache[email] = supervisor
          else
            supervisor = ScoutRFP::Models::Supervisor.new(first_name: nil, last_name: nil, email: nil)
          end
          supervisor
        end
      rescue NoMethodError
        ScoutRFP::Models::Supervisor.new(first_name: nil, last_name: nil, email: nil)
      end
    end
  end
end