require 'singleton'
require 'uri'
require 'rest-client'
require 'json'
module ScoutRFP
  module Client
    class ThunderTurtleAdapter
      include Singleton

      @@base_uri = "https://dataengineerinterview.thunderturtle.io/contacts/"
      def self.search(email)
        params = {email: email}
        get('search/', params)
      end

      def self.get(uri, params = nil)
        url = @@base_uri + uri
        headers = {
          'X-Api-Key': 'dMnFzZb7IW97UtogFllcH8nwo6MmbEqn2GorlCoe',
          params: params
        }
        JSON.parse RestClient.get(url, headers)
      end
    end
  end
end

