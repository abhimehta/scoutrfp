require 'singleton'
require 'redis'

module ScoutRFP
  module Client
    class RedisAdapter

      include Singleton

      attr_reader :redis

      def initialize
        @redis ||= Redis.new(host: '127.0.0.1', port: '6379')
      end

    end
  end
end


