#!/usr/bin/env ruby

require 'stringio'
require_relative 'app/lib/parser/contacts'
require_relative 'app/lib/parser/NAICS/code'
require_relative 'app/lib/queue/raw_contact'
require_relative 'app/lib/queue/unprocessable_contact'

module ScoutRFP
  module Main
    class << self
      def start(sector_titles)
        initialize_indexers

        p 'Step 2:'
        p 'Get Selected Sectors'
        @selected_sectors = ScoutRFP::Parser::NAICS.get_sector_code(sector_titles)
        initialize_queues
        initialize_ingestor(sectors: @selected_sectors)
      end

      def initialize_queues
        p 'Step 3:'

        p 'Initializing Queues'

        p 'Initializing RawContact Queue'
        ScoutRFP::Queue::RawContact.setup
        p 'Initializing UnprocessableContact Queue'
        ScoutRFP::Queue::UnprocessableContact.setup
      end

      def initialize_ingestor(sectors: nil)
        p 'Step 4'
        p 'Initialize Ingestion'
        ScoutRFP::Parser::Contact.process(sectors)
      end

      def initialize_indexers()
        p 'Step 1:'
        p 'Initializing NAICS Parser'

        ScoutRFP::Parser::NAICS.process
      end
    end
  end
end

sector_titles = $ARGV.compact.map(&:to_s).map(&:to_sym) || []

if sector_titles.length.zero?
  p 'No NAICS Sectors mentioned. Exiting.'
  exit!
else
  p "Processing for the following NAICS Sector Titles: #{sector_titles.join(',')}"
end



ScoutRFP::Main.start(sector_titles)
