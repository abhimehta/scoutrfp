## Author
Abhishek Mehta | me@abhimehta.com | (213)321-1850

## PreRequisite
Expects Redis to be installed and available at the following connection string:
`127.0.0.1:6379`

## Instructions

Goto the root of the application and run

`ruby main.rb`

It expects arguments for specific NAICS Sectors
If no sectors are provided, it would stop processing.

## Pending
Have not been able to create the actual Data on the API endpoints specified since I have been randomly getting 502 Errors 

## Completed
I have completed the parsing of contacts and pushing them to a RedisQueue (RAW_CONTACT_QUEUE). I expected a worker to pick up the contacts from queue and create contacts on the remote API